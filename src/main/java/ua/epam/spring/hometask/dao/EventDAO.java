package ua.epam.spring.hometask.dao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import ua.epam.spring.hometask.domain.Event;


//save, remove, getById, getByName, getAll
public class EventDAO {

	private static Map<Long, Event> events = new HashMap<>();

	public void setEvents(Map<Long, Event> events){
		this.events=events;
	}

	public Event save(Event event) {
        events.put(event.getId(), event);
        return event;
	}
	
	public void remove(Event event) {
        events.remove(event.getId());
	}
	
	
	public Event getById(Long id) {
        return events.get(id);
	}
	
	
	public Event getByName(String name) {
        return events.values().stream()
        		.filter(e->e.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
	}
	
	public Set<Event> getAll() {
        return events.values().stream().collect(Collectors.toSet());
	}

	public Set<Event> getForDateRange(LocalDate from, LocalDate to) {
        return events.values().stream()
        		.filter(e->e.getAirDates().higher(from.atStartOfDay())!=null && e.getAirDates().lower(to.atStartOfDay())!=null)
        		.collect(Collectors.toSet());
	}
	
	public Set<Event> getNextEvents(LocalDateTime to) {
		return getForDateRange(LocalDate.now(), to.toLocalDate());
	}
	
	
	
	
}
