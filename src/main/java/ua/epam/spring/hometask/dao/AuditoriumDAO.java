package ua.epam.spring.hometask.dao;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import ua.epam.spring.hometask.domain.Auditorium;

//getAll(), getByName()
public class AuditoriumDAO {
	
	private List<Auditorium> auditoriums;
	
	public AuditoriumDAO(List<Auditorium> auditoriums) {
		this.auditoriums=auditoriums;
	}
	
	public Auditorium getByName(String name) {
        return Optional.of(auditoriums).isPresent()
        		?auditoriums.stream().filter(a->a.getName().equalsIgnoreCase(name)).findFirst().orElse(null)
        		:null;
	}
	
	public Set<Auditorium> getAll() {
        return auditoriums.stream().collect(Collectors.toSet());
	}
	

}
