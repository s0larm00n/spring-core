package ua.epam.spring.hometask.dao;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import ua.epam.spring.hometask.domain.User;


//save, remove, getById, getUserByEmail, getAll
public class UserDAO {
	
	private static Map<Long, User> users = new HashMap<>();

	public Long count(){
		Number size = users.size();
		return size.longValue();
	}

	public void setUsers(HashMap users){
		this.users=users;
	}
	
	public User save(User user) {
		users.put(user.getId(), user);
		return user;
	}
	
	public void remove(User user) {
		users.remove(user.getId(), user);
	}
	
	
	public User getById(Long id) {
		return users.get(id);
	}
	
	public User getUserByEmail(String email) {
		return users.values()
			.stream()
			.filter(u->u.getEmail().equalsIgnoreCase(email)).findFirst().orElse(null);
	}
	
	public Set<User> getAll() {
		return users.values().stream().collect(Collectors.toSet());
	}

}
