package ua.epam.spring.hometask;

import org.springframework.cglib.core.Local;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.EventRating;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.AuditoriumService;
import ua.epam.spring.hometask.service.BookingService;
import ua.epam.spring.hometask.service.DiscountService;
import ua.epam.spring.hometask.service.EventService;
import ua.epam.spring.hometask.service.UserService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static ua.epam.spring.hometask.domain.EventRating.*;

public class ElApp {
	
	private User currentUser;
	private UserService userService;
	private EventService eventService;
	private BookingService bookingService;
	private AuditoriumService auditoriumService;
	Scanner in;

	public ElApp(
			UserService userService,
			EventService eventService,
			BookingService bookingService,
			AuditoriumService auditoriumService
			) {
		this.userService=userService;
		this.eventService=eventService;
		this.bookingService=bookingService;
		this.auditoriumService=auditoriumService;
		this.in = new Scanner(System.in);
	}
	
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ApplicationContext ctx =
				new ClassPathXmlApplicationContext(
				"spring.xml");

		ElApp app = (ElApp) ctx.getBean("app");

		app.exec();


		System.out.println(app.auditoriumService.getAll());

		((ClassPathXmlApplicationContext) ctx).close();
	}
	
	
	public void exec(){
		replica("--- Welcome to Cinema Client 0.2 beta ---\n");
		int stage=0;
		while(true){
			switch (stage){
				case 0: replica("You are now in main menu; proceed to admin (1) or user (2) menu or exit (3) >> ");
						stage=in.nextInt();
						break;
				case 1: adminMenuContext();
						stage=0;
						break;
				case 2: userLogin();
						stage=0;
						break;
				case 3: return;
				default:stage=0;
						break;
			}


		}
	}

	public void userLogin(){
		replica("Please enter your email address: ");
		//String mail=in.next();
		String mail="mcurie@gmail.com";
		User usr=userService.getUserByEmail(mail);
		if(usr==null){
			replica("No user found. Please enter your data for registration:\n");
			String fname,lname,bd,id;
			replica("First name: ");
			fname=in.next();
			replica("Last name: ");
			lname=in.next();
			replica("Birthday (format example: 2016-03-24): ");
			bd=in.next();
			bd=bd+" 00:00";
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
			LocalDateTime bdate = LocalDateTime.parse(bd, formatter);
			usr=new User();
			usr.setStats(userService.count()+1,fname,lname,mail,bdate);
			userService.save(usr);
		}
		currentUser=usr;
		userMenuContext();
	}

	public void userMenuContext(){
		replica("[Now entering user mode]\n");
		int stage=0;
		while(true){
			switch (stage){
				case 0: replica("[Logged in as "+currentUser.getEmail()+"]\nYou are now in user menu; select action:\n1. See events\n2. Exit\n>> ");
						stage=in.nextInt();
						break;
				case 1: showEvents();
						stage=0;
						break;
				case 2: return;
				default:stage=0;
						break;
			}
		}


		//auditoriumService.getAll()

	}

	public void showEvents(){
		ArrayList<Event> events = new ArrayList<>(eventService.getAll());
		int k=1;
		for(Event ev:events){
			replica(k+": "+ev.getName()+": ("+ev.getRating()+")\n");
			System.out.println(ev.getAirDates());
			k++;
		}
		replica("Would you like to book tickets for event?\n Enter 0 to exit, nubmer of event to book >> ");
		k=in.nextInt();
		if(k==0){
			replica("Very well. Returning to user menu...\n");
			return;
		}
		else{
			if(events.size()<k||k<0){
				replica("No such event! Try again later. Returning to user menu...\n");
				return;
			}
            booking(events.get(k-1));
		}
	}

	public void booking(Event ev){
		replica("Enter seats (format: 3,4,20 or just 20) >> ");
		String inp=in.next();
		String[] subStr;
		String delimeter = ",";
		subStr = inp.split(delimeter);
		Set<Long> seats=new HashSet<>();
		for(int i=0;i<subStr.length;i++){
			seats.add(Long.parseLong(subStr[i]));
		}
		replica("Desired date (format example: 2016-03-24 11:30): ");
		inp=in.next();
		String inp2=in.next();
		inp=inp+" "+inp2;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime bdate = LocalDateTime.parse(inp, formatter);
		System.out.println(bookingService.getTicketsPrice(ev,bdate,currentUser,seats));

		replica("Do you wish to proceed with booking? 1 - yes, else - no: ");
		int response=in.nextInt();

		if(response==1){
			Set<Ticket> sett=new HashSet<>();
			Long point=bookingService.getVacantId();
			for(Long siat: seats){
				Ticket temp=new Ticket(currentUser,ev,bdate,siat);
				temp.setId(point);
				sett.add(temp);
				point++;
			}
			bookingService.bookTickets(sett);
			replica("Operation completed.\n");
		}
		else{
			replica("Operation aborted.\n");
		}
	}

	public void showUsers(){
		ArrayList<User> users = new ArrayList<>(userService.getAll());
		for(User us:users){
			replica(us.getFirstName()+" "+us.getLastName()+"\n");
		}
	}

	public void adminMenuContext(){
		replica("[Now entering admin mode]\n");
		int stage=0;
		while(true){
			switch (stage){
				case 0: replica("You are now in admin menu; select action:\n1. Show purchased tickets\n2. Add an event\n3. Exit\n>> ");
						stage=in.nextInt();
						break;
				case 1: showTickets();
						stage=0;
						break;
				case 2: addEvent();
						stage=0;
						break;
				case 3: return;
				default:stage=0;
						break;
			}
		}
	}

	public void showTickets(){
		replica("Choose an event: \n");
		HashSet<Event> local=(HashSet<Event>)eventService.getAll();
		int i=1;
		for(Event ev:local){
			System.out.println(i+". "+ev.getName());
			i++;
		}
		int res=in.nextInt(),k=1;
		Event theNeededEvent=new Event();
		for(Event ev:local){
			if(k==res){
				theNeededEvent=ev;
				break;
			}
			k++;
		}

		replica("Desired date (format example: 2016-03-24 11:30): ");
		String inp=in.next();
		String inp2=in.next();
		inp=inp+" "+inp2;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime bdate = LocalDateTime.parse(inp, formatter);
		HashSet<Ticket> ticketio=(HashSet<Ticket>)bookingService.getPurchasedTicketsForEvent(theNeededEvent,bdate);
		for(Ticket tic:ticketio){
			System.out.println(tic.toString());
		}
	}

	public void addEvent(){
		Event newEvent=new Event();
			System.out.println("Enter properties for new event one by one:");

			replica("Event title: ");
			String nam=in.next();
			newEvent.setName(nam);

			replica("Base price: ");
			double pric=in.nextDouble();
			newEvent.setBasePrice(pric);

			replica("Event rating (0-L,1-M,2-H): ");
			int rat=in.nextInt();
			EventRating rating;
			switch (rat){
				case 1: rating=MID;
				case 2: rating=HIGH;
				default: rating=LOW;
			}
			newEvent.setRating(rating);

			replica("Event date (format example: 2016-03-24 11:30): ");
			String inp=in.next();
			String inp2=in.next();
			inp=inp+" "+inp2;
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
			LocalDateTime bdate = LocalDateTime.parse(inp, formatter);
			newEvent.addAirDateTime(bdate);

			eventService.save(newEvent);
	}

	public static void replica(String rep){
		System.out.print(rep);
	}

}
