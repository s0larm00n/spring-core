package ua.epam.spring.hometask.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Set;

import ua.epam.spring.hometask.dao.EventDAO;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.service.EventService;


//save, remove, getById, getByName, getAll
//getForDateRange(from, to) - returns events for specified date range (OPTIONAL)
//getNextEvents(to) - returns events from now till the ‘to’ date (OPTIONAL)
public class EventServiceImpl implements EventService {
	
	private EventDAO eventDAO;
	
	public EventServiceImpl(EventDAO eventDAO) {
		this.eventDAO=eventDAO;
	}

	@Override
	public Event save(Event e) {
		return this.eventDAO.save(e);
	}

	@Override
	public void remove(Event e) {
		this.eventDAO.remove(e);

	}

	@Override
	public Event getById(Long id) {
		return this.eventDAO.getById(id);
	}

	@Override
	public Collection<Event> getAll() {
		return this.eventDAO.getAll();
	}

	@Override
	public Event getByName(String name) {
		return this.eventDAO.getByName(name);
	}

	@Override
	public Set<Event> getForDateRange(LocalDate from, LocalDate to) {
		return this.eventDAO.getForDateRange(from,to);
	}

	@Override
	public Set<Event> getNextEvents(LocalDateTime to) {
		// TODO Auto-generated method stub
		return null;
	}

}
