package ua.epam.spring.hometask.service.impl.discount;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;

public class BirthdayDiscountStrategy implements DiscountStrategy {

	private static final int PER_DISCOUNT_PERCENT=10;
	private static final int BEWTEEN_BIRTHDAY_DAYS=5;
	
	
	@Override
	public byte getFlatDiscountPercent(User user, Event event, LocalDateTime airDateTime, long numberOfTickets) {
		
		if(user==null) {return 0;} 
		
		LocalDate birthday = user.getBirthday().toLocalDate();
		LocalDate airDate = airDateTime.toLocalDate();
		LocalDate curBirthday = birthday.withYear(airDate.getYear());
//		Period periodToNext = Period.between(airDate, curBirthday); 
//		periodToNext.getDays() 
		long between = Math.abs(ChronoUnit.DAYS.between(airDate, curBirthday));
		//between = start.until(end, thisUnit);
		if(between<=BEWTEEN_BIRTHDAY_DAYS) {
			return PER_DISCOUNT_PERCENT;
		}else {
			return 0;
		}
		
	}



}
