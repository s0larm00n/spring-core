package ua.epam.spring.hometask.service.impl;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

import ua.epam.spring.hometask.dao.TicketDAO;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.EventRating;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.BookingService;
import ua.epam.spring.hometask.service.DiscountService;

public class BookingServiceImpl implements BookingService {
	
	private DiscountService discountService;
	private TicketDAO ticketDAO;
	
    public BookingServiceImpl(DiscountService discountService,TicketDAO td) {
    	this.discountService = discountService;
        this.ticketDAO=td;
    }

    
	//User is needed to calculate discount (see below)
	//Event is needed to get base price, rating
	//Vip seats should cost more than regular seats (For example, 2xBasePrice)
	//All prices for high rated movies should be higher (For example, 1.2xBasePrice)
	@Override
	public double getTicketsPrice(Event event, LocalDateTime dateTime, User user, Set<Long> seats) {
		
		double basePrice=event.getBasePrice();
		double ratedPrice=event.getRating()==EventRating.HIGH?basePrice*1.2:basePrice;
		double totalPrice;
		try {
			totalPrice = seats.stream()
					.mapToDouble(seat -> event.getAuditoriums().get(dateTime).getVipSeats().contains(seat) ? ratedPrice * 2 : ratedPrice)
					.reduce(0.0, (acc, seatPrice) -> {
						return acc += seatPrice;
					});
		}
		catch(NullPointerException e){
			System.out.println("No such date for this event!");
			return 0;
		}
		//todo:
		//there is a problem here, the 10th seat may be a Vip seat or not  :(
		//the DiscountService interface should be refactored
		//System.out.println("gjrbfdkvd");
		byte discount = discountService.getDiscount(user, event, dateTime, seats.size());

		System.out.println(totalPrice);
		return totalPrice*(1-(double)discount/100);
	}
	

	@Override
	public void bookTickets(Set<Ticket> tickets) {
		for(Ticket tic: tickets) {
			this.ticketDAO.save(tic);
		}
	}

	@Override
	public Set<Ticket> getPurchasedTicketsForEvent(Event event, LocalDateTime dateTime) {
//		eventDAO.getNextEvents(dateTime)
		return ticketDAO.getAll().stream()
				.filter(t->t.getEvent().getId()==event.getId() && t.getDateTime().isEqual(dateTime))
				.collect(Collectors.toSet());
	}


	public Long getVacantId(){
    	Number X = ticketDAO.getAll().size()+1;
    	return X.longValue();
	}
	

}
