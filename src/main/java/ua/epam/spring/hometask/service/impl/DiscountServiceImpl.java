package ua.epam.spring.hometask.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.DiscountService;
import ua.epam.spring.hometask.service.impl.discount.DiscountStrategy;

public class DiscountServiceImpl implements DiscountService {

	private List<DiscountStrategy> discountStrategies;
	
    public DiscountServiceImpl(List<DiscountStrategy> discountStrategies) {
        this.discountStrategies = discountStrategies;
    }
    
    
	@Override
	public byte getDiscount(User user, Event event, LocalDateTime airDateTime, long numberOfTickets) {
		byte resDis=0;
		for(DiscountStrategy str:discountStrategies){
			byte temp=str.getFlatDiscountPercent(user,event,airDateTime,numberOfTickets);
			if(temp>resDis)
				resDis=temp;
		}
		return resDis;
	}
	
	
	
}
