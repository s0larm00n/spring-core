package ua.epam.spring.hometask.service.impl;

import java.util.Set;

import ua.epam.spring.hometask.dao.AuditoriumDAO;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.service.AuditoriumService;

public class AuditoriumServiceImpl implements AuditoriumService {

	private AuditoriumDAO auditoriumDAO;
	
	public AuditoriumServiceImpl(AuditoriumDAO auditoriumDAO) {
		this.auditoriumDAO=auditoriumDAO;
	}
	
	@Override
	public Set<Auditorium> getAll() {
		// TODO Auto-generated method stub
		return auditoriumDAO.getAll();
	}

	@Override
	public Auditorium getByName(String name) {
		// TODO Auto-generated method stub
		return auditoriumDAO.getByName(name);
	}

}
