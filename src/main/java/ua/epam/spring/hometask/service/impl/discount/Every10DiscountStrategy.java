package ua.epam.spring.hometask.service.impl.discount;

import java.time.LocalDateTime;

import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;

public class Every10DiscountStrategy implements DiscountStrategy {

	private static final int PER_DISCOUNT_PERCENT=5;
	
	@Override
	public byte getFlatDiscountPercent(User user, Event event, LocalDateTime airDateTime, long numberOfTickets) {
		long discountNums=numberOfTickets/10;
		return (byte) (discountNums*PER_DISCOUNT_PERCENT);
	}




}
