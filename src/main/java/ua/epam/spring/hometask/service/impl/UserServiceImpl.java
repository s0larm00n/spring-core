package ua.epam.spring.hometask.service.impl;

import java.util.Collection;

import ua.epam.spring.hometask.dao.UserDAO;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.UserService;

public class UserServiceImpl implements UserService {

	private UserDAO userDAO;

	public Long count(){
		return userDAO.count();
	}

    public UserServiceImpl(UserDAO dao) {
        this.userDAO = dao;
    }
	
	@Override
	public User save(User u) {
		return userDAO.save(u);
	}

	@Override
	public void remove(User u) {
		userDAO.remove(u);
	}

	@Override
	public User getById(Long id) {
		return userDAO.getById(id);
	}

	@Override
	public Collection<User> getAll() {
		// TODO Auto-generated method stub
		return userDAO.getAll();
	}

	@Override
	public User getUserByEmail(String email) {
		// TODO Auto-generated method stub
		return userDAO.getUserByEmail(email);
	}

}
